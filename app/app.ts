import isRoot from 'is-root';
import { CellsimRest } from './cellsim/cellsim-rest';
import { SystemInteropUtils } from './system-interop-utils';
import * as yargs from 'yargs';
import { CellsimRestConfig } from './cellsim/cellsim-rest.config';
import { promises as fs } from 'fs';
import decomment from 'decomment';

const APPNAME = 'Cellsim Controller';
const argv = initArguments();
function initArguments() {
  return yargs
    .option('config', {
      alias: 'c',
      description: 'Set the path to a config file.',
      type: 'string',
      default: './app/config.jsonc',
    })
    .option('dev', {
      alias: 'd',
      description: 'Run in development mode',
      type: 'boolean',
    })
    .help('h')
    .alias('help', 'h')
    .epilog('Copyright 2020 by Martin Meszaros').argv;
}

function exitIfNotRoot() {
  if (!isRoot()) {
    console.error(`${APPNAME} needs to be started as root!`);
    if (argv.dev) {
      console.warn('Running in dev-mode so not exiting now.');
    } else {
      process.exit(1);
    }
  }
}

function removeTrailingCommaFromJsonString(json: string): string {
  const regex = /\,(?!\s*?[\{\[\"\'\w])/g;
  return json.replace(regex, '');
}

async function main() {
  exitIfNotRoot();

  let config: CellsimRestConfig;

  const defaultConfig = CellsimRest.getDefaultConfig();
  if (!(await SystemInteropUtils.canReadFile(argv.config))) { 
    console.error('Can not read config file. Using defaults.');
    config = defaultConfig;
  } else {
    console.log(`Opening config file "${argv.config}"`);
    const loadedConfig = (await fs.readFile(argv.config)).toString();
    const cleanedConfig = removeTrailingCommaFromJsonString(decomment(loadedConfig));
    const parsedConfig = JSON.parse(cleanedConfig);
    config = { ...defaultConfig, ...parsedConfig };
  }

  try {
    const cellsimRest = new CellsimRest(config);
    await cellsimRest.listen();
  } catch (e) {
    console.log('Failed running Cellsim REST:', e);
  }
}

main();
