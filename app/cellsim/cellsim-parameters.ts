export interface CellsimParameters {
  cellsimExecutablePath: string;
  internetFacingInterface: string;
  clientFacingInterface: string;
}
