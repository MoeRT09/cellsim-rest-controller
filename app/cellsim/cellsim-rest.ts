import express, { Request, Response } from 'express';
import https from 'https';
import http from 'http';
import cors from 'cors';
import { promises as fs } from 'fs';
import { CellsimController } from './cellsim-controller';
import { CellsimRestConfig } from './cellsim-rest.config';
import { SystemInteropUtils } from '../system-interop-utils';
import * as HttpStatus from 'http-status-codes';
import * as path from 'path';

export class CellsimRest {
  private app: express.Application;
  private controller: CellsimController;

  private config: CellsimRestConfig;
  private httpServer?: http.Server;
  private httpsServer?: https.Server;

  public allowedTraceFileExtensions = ['up', 'down', 'pps'];

  constructor(config: CellsimRestConfig) {
    this.app = express();

    this.app.use(cors());
    this.config = CellsimRest.mergeConfig(config);
    console.log('Cellsim REST is using the following config:', config);
    this.controller = new CellsimController(this.config.internetFacingInterface, this.config.clientFacingInterface, this.config?.cellsimPath);

    this.controller.logPath = this.config.logDirectory;
    this.controller.automaticallyToggleEthernetBridge = !!config?.toggleEthernetBridge;
    if (config?.bridgeName) {
      this.controller.bridgeName = config.bridgeName;
    }
    this.setupHandlers();
  }

  private setupHandlers() {
    this.app.route('/start').post(this.onStart.bind(this)).all(this.methodNotAllowed.bind(this));
    this.app.route('/stop').put(this.onStop.bind(this)).all(this.methodNotAllowed.bind(this));
    this.app.route('/traces').get(this.onTraces.bind(this)).all(this.methodNotAllowed.bind(this));
    this.app.route('/status').get(this.onStatus.bind(this)).all(this.methodNotAllowed.bind(this));
  }

  public async listen() {
    await this.prepareServer();
    if (this.httpServer) {
      this.httpServer.listen(this.config.listenPort, () => {
        console.log(`Cellsim REST API listening on HTTP port ${this.config.listenPort}!`);
      });
    }
    if (this.httpsServer) {
      this.httpsServer.listen(this.config.listenPortHttps, () => {
        console.log(`Cellsim REST API listening on HTTPS port ${this.config.listenPortHttps}!`);
      });
    }
  }

  private async prepareServer() {
    if (this.config.listenPort) {
      this.httpServer = http.createServer(this.app);
    }
    if (this.config.listenPortHttps) {
      const canReadCert = this.config.sslCertificate ? await SystemInteropUtils.canReadFile(this.config.sslCertificate) : false;
      const canReadKey = this.config.sslPrivateKey ? await SystemInteropUtils.canReadFile(this.config.sslPrivateKey) : false;

      if (!canReadCert || !canReadKey || !this.config.sslPrivateKey || !this.config.sslCertificate) {
        throw new Error('Please provide a valid path to an SSL private key and certificate in the config.');
      }
      const privateKey = await fs.readFile(this.config.sslPrivateKey);
      const cert = await fs.readFile(this.config.sslCertificate);

      this.httpsServer = https.createServer(
        {
          cert: cert,
          key: privateKey,
        },
        this.app
      );
    }
  }

  private static async handleRequest(res: Response, handler: () => Promise<any>) {
    try {
      const result = await handler();
      console.log('Sending response:', { statusCode: res.statusCode, result: result });
      res.send(JSON.stringify(result));
    } catch (ex) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      console.log('Sending response:', { statusCode: res.statusCode, result: ex });
      res.send(JSON.stringify({ error: ex?.message ? ex.message : ex }));
    }
  }

  private methodNotAllowed(req: Request, res: Response) {
    CellsimRest.handleRequest(res, async () => {
      //Set the 'Allow' header according to RFC2616
      const allowedMethods = Object.keys(req.route.methods).filter((method) => method !== '_all');
      res.set('Allow', allowedMethods.join(', ').toUpperCase());
      res.status(HttpStatus.METHOD_NOT_ALLOWED);
    });
  }

  private async onStart(req: Request, res: Response) {
    CellsimRest.handleRequest(res, async () => {
      const upTrace = req.query.uplinkTrace?.toString();
      const downTrace = req.query.downlinkTrace?.toString();
      const lossrate = req.query.lossrate;
      const logTag = req.query.logTag;

      if (!upTrace || !downTrace) {
        res.status(HttpStatus.BAD_REQUEST);
        return {
          error:
            'Specify an up- and downlink trace as follows: "/start?uplinkTrace=trace.up&downlinkTrace=trace.down". Issue "/traces" to get a list of available choices.',
        };
      }

      const fullUptracePath = this.getFullTracePath(upTrace);
      const fullDowntracePath = this.getFullTracePath(downTrace);

      const uptraceReadable = await SystemInteropUtils.canReadFile(fullUptracePath);
      const downtraceReadable = await SystemInteropUtils.canReadFile(fullDowntracePath);

      if (!uptraceReadable || !downtraceReadable) {
        res.status(HttpStatus.BAD_REQUEST);
        return { error: `Can't open the ${uptraceReadable ? 'downlink' : 'uplink'} trace file.` };
      }

      if (lossrate !== undefined && !CellsimRest.isValidLossrate(Number(lossrate))) {
        res.status(HttpStatus.BAD_REQUEST);
        return { error: 'The lossrate must be a value between 0 and 1.' };
      }

      const lossrateOrZero = lossrate ? Number(lossrate) : 0;
      const tagOrEmptyString = logTag ? logTag.toString() : '';

      await this.controller.run(fullUptracePath, fullDowntracePath, lossrateOrZero, tagOrEmptyString);

      return {
        running: this.controller.isRunning,
        uplinkTrace: fullUptracePath,
        downlinkTrace: fullDowntracePath,
        lossrate: lossrateOrZero,
        logTag: logTag,
      };
    });
  }

  private static isValidLossrate(value: any): boolean {
    if (typeof value !== 'number') {
      return false;
    }
    if (value < 0 || value > 1 || Number.isNaN(value)) {
      return false;
    }

    return true;
  }

  private async onStop(req: Request, res: Response) {
    CellsimRest.handleRequest(res, async () => {
      const code = await this.controller.stop();

      return { running: this.controller.isRunning, exitCode: code };
    });
  }

  private async onTraces(req: Request, res: Response) {
    CellsimRest.handleRequest(res, async () => {
      const traces = await this.getAvailableTraces();

      return traces;
    });
  }

  private async onStatus(req: Request, res: Response) {
    CellsimRest.handleRequest(res, async () => {
      return { running: this.controller.isRunning };
    });
  }

  private static mergeConfig(customConfig: CellsimRestConfig): CellsimRestConfig {
    const defaultConfig = CellsimRest.getDefaultConfig();
    return { ...defaultConfig, ...customConfig };
  }

  public async getAvailableTraces() {
    if (!this.config.traceDirectory) {
      return [];
    }
    if (!(await SystemInteropUtils.canReadFile(this.config.traceDirectory))) {
      throw new Error(`Can't read from the trace file directory at ${this.config.traceDirectory}`);
    }
    return await SystemInteropUtils.readFilesInDirectory(this.config.traceDirectory, this.allowedTraceFileExtensions);
  }

  private getFullTracePath(filename: string) {
    const directory = this.config?.traceDirectory ? this.config.traceDirectory : './';

    const tracePathWithFilename = path.join(directory, filename);
    return path.resolve(tracePathWithFilename);
  }

  public static getDefaultConfig(): CellsimRestConfig {
    return {
      clientFacingInterface: 'eth0',
      internetFacingInterface: 'eth1',
      logDirectory: './logs',
      traceDirectory: './traces',
      listenPort: 3000,
      toggleEthernetBridge: false,
      bridgeName: 'br0',
    };
  }
}
