import { promises as fs, constants as fsConstants } from 'fs';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';
import readline from 'readline';
import { CellsimParameters } from './cellsim-parameters';
import { SystemInteropUtils, TaskInfo } from '../system-interop-utils';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as path from 'path';

export class CellsimRunner {
  private static readonly CELLSIM_PROCESS_NAME = 'cellsim';
  private runningCellsimInstance?: ChildProcessWithoutNullStreams;
  private cellsimExecutablePath: Promise<string>;

  public onDownlinkMessage$ = new Subject<string>();
  public onStdErr$ = new Subject<string>();
  public onUplinkMessage$ = new Subject<string>();
  public onStdOut$ = new Subject<string>();
  public onClose$ = new Subject<number>();

  public logPath?: string;
  public logTag?: string;

  public get isRunning() {
    return !!this.runningCellsimInstance;
  }

  constructor(private parameters: CellsimParameters) {
    this.cellsimExecutablePath = this.getCellsimExecutablePath();
  }

  /** Checks if Cellsim is running.
   *
   * @returns Promise of TaskInfo[] for every running Cellsim instance. Otherwise false
   */

  public static async isCellsimRunning(): Promise<TaskInfo[] | false> {
    const tasks = await SystemInteropUtils.getRunningProcesses();
    const cellsimTasks = tasks.filter((task) => task.name === this.CELLSIM_PROCESS_NAME);

    return cellsimTasks.length > 0 ? cellsimTasks : false;
  }

  public isCellsimAvailableInCustomPath(): Promise<boolean> {
    return SystemInteropUtils.commandExists(this.parameters.cellsimExecutablePath);
  }

  private static isCellsimAvailable(path: string): Promise<boolean> {
    return SystemInteropUtils.commandExists(path);
  }

  public static isCellsimAvailableInPath(): Promise<boolean> {
    return CellsimRunner.isCellsimAvailable(CellsimRunner.CELLSIM_PROCESS_NAME);
  }

  public async getCellsimExecutablePath(): Promise<string> {
    if (await this.isCellsimAvailableInCustomPath()) {
      return this.parameters.cellsimExecutablePath;
    } else if (await CellsimRunner.isCellsimAvailableInPath()) {
      return CellsimRunner.CELLSIM_PROCESS_NAME;
    } else {
      throw new Error('The cellsim executable cannot be found. Make it available in PATH or set the corresponding variable.');
    }
  }

  private static generateLogFilename(prefix: string, tag: string = ''): string {
    return `${CellsimRunner.CELLSIM_PROCESS_NAME}_${tag ? tag + '_' : ''}${prefix}_${new Date().toISOString()}.log`;
  }

  public async run(uplinkTracePath: string, downlinkTracePath: string): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, logTag: string): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, lossrate: number): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, lossrate: number, logTag: string): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, lossrateOrTag?: number | string, logTag?: string): Promise<void> {
    const lossrate = typeof lossrateOrTag === 'number' ? lossrateOrTag : 0;
    const tag = typeof lossrateOrTag === 'string' ? lossrateOrTag : logTag ? logTag : "";

    const isRunning = await CellsimRunner.isCellsimRunning();
    if (isRunning) {
      throw new Error(
        `Cellsim is already running (PID: ${isRunning.map((instance) => instance.pid).join(', ')}). Only one instance can run at the time.`
      );
    }
    await CellsimRunner.throwIfInterfacesDoNotExist(this.parameters.clientFacingInterface, this.parameters.internetFacingInterface);
    await fs.access(uplinkTracePath, fsConstants.R_OK);
    await fs.access(downlinkTracePath, fsConstants.R_OK);

    if (this.logPath) {
      const uplinkLogPath = this.getFullLogPath('up', tag);
      const downlinkLogPath = this.getFullLogPath('down', tag);
      await SystemInteropUtils.throwIfFileCantBeCreated(uplinkLogPath, downlinkLogPath);
    }

    const cellsimParams = this.getCellsimParameterArray(uplinkTracePath, downlinkTracePath, lossrate, tag);
    this.runningCellsimInstance = spawn(await this.cellsimExecutablePath, cellsimParams);

    readline
      .createInterface({
        input: this.runningCellsimInstance.stdout,
        terminal: false,
      })
      .on('line', this.onStdOut.bind(this));

    readline
      .createInterface({
        input: this.runningCellsimInstance.stderr,
        terminal: false,
      })
      .on('line', this.onStdErr.bind(this));

    this.onClose$ = new Subject<number>();
    this.runningCellsimInstance.on('close', this.onClose.bind(this));

    this.onStdOut$.pipe(takeUntil(this.onClose$)).subscribe(console.log);

    this.onStdErr$.pipe(takeUntil(this.onClose$)).subscribe(console.error);
  }

  private getFullLogPath(prefix: string, tag: string = "") {
    if (this.logPath) return path.resolve(path.join(this.logPath, CellsimRunner.generateLogFilename(prefix, tag)));

    return path.resolve(CellsimRunner.generateLogFilename(prefix, tag));
  }

  public enableLogOutput() {
    this.onUplinkMessage$.pipe(takeUntil(this.onClose$)).subscribe(console.log);

    this.onDownlinkMessage$.pipe(takeUntil(this.onClose$)).subscribe(console.error);
  }

  public stop() {
    return new Promise<number>((resolve, reject) => {
      if (!this.runningCellsimInstance) {
        console.log('Cellsim is not running.');
        resolve();
        return;
      }
      this.runningCellsimInstance.on('close', (code) => resolve(code));
      this.runningCellsimInstance.kill();
    });
  }

  private onStdOut(line: string) {
    // Cellsims stdout stream refers to the uplink
    this.emitLine(line, this.onStdOut$, this.onUplinkMessage$);
  }

  private onStdErr(line: string) {
    // Cellsims stderr stream refers to the downlink
    this.emitLine(line, this.onStdErr$, this.onDownlinkMessage$);
  }

  private static outputMatcher = new RegExp(/^\d+\s[-+#]\s\d+/m);
  private isLogOutput(line: string) {
    return !!line.match(CellsimRunner.outputMatcher);
  }

  private onClose(code: number) {
    console.log('cellsim exited with code', code);
    this.runningCellsimInstance = undefined;
    this.onClose$.next(code);
    this.onClose$.complete();
  }

  private emitLine(line: string, systemSubject: Subject<string>, logSubject: Subject<string>) {
    if (!line) return;

    if (this.isLogOutput(line)) {
      logSubject.next(line);
    } else {
      systemSubject.next(line);
    }
  }

  private getCellsimParameterArray(uplinkTrace: string, downlinkTrace: string, lossrate: number, tag: string = "") {
    const params = [uplinkTrace, downlinkTrace, lossrate.toString(), this.parameters.internetFacingInterface, this.parameters.clientFacingInterface];

    if (this.logPath) {
      const uplinkLog = this.getFullLogPath('up', tag);
      const downlinkLog = this.getFullLogPath('down', tag);
      console.log('Will save uplink log to: ', path.resolve(uplinkLog));
      console.log('Will save downlink log to: ', path.resolve(downlinkLog));
      params.push(uplinkLog, downlinkLog); 
    }

    return params;
  }

  private static async throwIfInterfacesDoNotExist(...interfaces: string[]) {
    for (const int of interfaces) {
      if (!(await CellsimRunner.interfaceExists(int))) {
        throw new Error(`The specified interface ${int} does not exist on the system`);
      }
    }
  }

  public static async interfaceExists(interfaceName: string): Promise<boolean> {
    const ifconfigAvailable = await SystemInteropUtils.commandExists('ifconfig');
    if (!ifconfigAvailable) {
      console.warn("Can't check for available interfaces, as the 'ifconfig' command does not exist");
      return false;
    }
    const regex = new RegExp(`^${interfaceName}:\\s`, 'm');
    const interfaceAvailable = await SystemInteropUtils.commandOutputMatchesRegex('ifconfig', regex);

    return interfaceAvailable;
  }
}
