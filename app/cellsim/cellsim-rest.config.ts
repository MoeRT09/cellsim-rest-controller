export interface CellsimRestConfig {
  listenPort?: number;
  listenPortHttps?: number;
  sslPrivateKey?: string;
  sslCertificate?: string;
  traceDirectory?: string;
  logDirectory?: string;
  internetFacingInterface: string;
  clientFacingInterface: string;
  cellsimPath?: string;
  toggleEthernetBridge?: boolean;
  bridgeName?: string;
}
