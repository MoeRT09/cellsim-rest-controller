import { SystemInteropUtils } from '../system-interop-utils';
import { CellsimRunner } from './cellsim-runner';

export class CellsimController extends CellsimRunner {
  public bridgeName = 'br0';
  public automaticallyToggleEthernetBridge = false;

  constructor(private internetFacingInterfaceName: string, private clientFacingInterfaceName: string, cellsimPath: string = '') {
    super({
      cellsimExecutablePath: cellsimPath,
      clientFacingInterface: clientFacingInterfaceName,
      internetFacingInterface: internetFacingInterfaceName,
    });

    ['SIGINT', 'SIGTERM', 'SIGQUIT'].forEach((signal) => {
      process.on(signal, this.onExit.bind(this));
    });
  }

  public async run(uplinkTracePath: string, downlinkTracePath: string): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, logTag: string): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, lossrate: number): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, lossrate: number, logTag: string): Promise<void>;
  public async run(uplinkTracePath: string, downlinkTracePath: string, lossrateOrTag?: number | string, logTag?: string): Promise<void> {
    const lossrate = typeof lossrateOrTag === 'number' ? lossrateOrTag : 0;
    const tag = typeof lossrateOrTag === 'string' ? lossrateOrTag : logTag ? logTag : '';
    await this.configureInterfaces();

    if (this.automaticallyToggleEthernetBridge) {
      console.log(`Disabling bridge ${this.bridgeName}`);
      await this.bridgeDown();
    }

    await super.run(uplinkTracePath, downlinkTracePath, lossrate, tag);

    console.log('Cellsim is running');
  }

  public async stop() {
    const code = await super.stop();

    if (this.automaticallyToggleEthernetBridge) {
      console.log(`Enabling bridge ${this.bridgeName}`);
      await this.bridgeUp();
    }

    return code;
  }

  private async configureInterfaces() {
    const networkInterfaces = [this.clientFacingInterfaceName, this.internetFacingInterfaceName];
    for (const int of networkInterfaces) {
      await CellsimController.enablePromiscousMode(int);
      await CellsimController.disableOffload(int);
    }
  }

  private static async enablePromiscousMode(networkInterface: string) {
    if (!(await SystemInteropUtils.commandExists('ifconfig'))) {
      throw new Error("Failed performing necessary configuration of Ethernet interfaces, as the 'ifconfig' command is unavailable");
    }

    await SystemInteropUtils.execPromise(`ifconfig ${networkInterface} up promisc`);
  }

  private static async disableOffload(networkInterface: string) {
    if (!(await SystemInteropUtils.commandExists('ethtool'))) {
      throw new Error("Failed performing necessary configuration of Ethernet interfaces, as the 'ethtool' command is unavailable");
    }
    // generic segmentation offload (GSO), TCP Segmentation Offload (TSO), large receive offload (LRO)
    await SystemInteropUtils.execPromise(`ethtool --offload ${networkInterface} gso off tso off gro off`);
  }

  public async isBridgeAvailable(): Promise<boolean> {
    const brctlAvailable = await SystemInteropUtils.commandExists('brctl');
    if (!brctlAvailable) {
      console.warn("Can't handle ethernet bridge as the 'brctl' command is not available");
      return false;
    }
    const regex = new RegExp(`^${this.bridgeName}\\s`, 'm');
    const bridgeAvailable = await SystemInteropUtils.commandOutputMatchesRegex('brctl show', regex);

    return bridgeAvailable;
  }

  public async bridgeUp(): Promise<void> {
    await this.handleBridge(true);
  }

  public async bridgeDown(): Promise<void> {
    await this.handleBridge(false);
  }

  private async handleBridge(bringUp: boolean): Promise<void> {
    const bridgeAction = bringUp ? 'up' : 'down';

    if (!(await SystemInteropUtils.commandExists('ifconfig'))) {
      console.error(`Can't bring bridge ${this.bridgeName} ${bridgeAction} as the "ifconfig" command is not available.`);
      return;
    }
    try {
      await SystemInteropUtils.execPromise(`ifconfig ${this.bridgeName} ${bridgeAction}`);
    } catch (e) {
      console.error(`Failed bringing bridge ${bridgeAction}: `, e);
    }
  }

  private onExit() {
    try {
      if (this.automaticallyToggleEthernetBridge) {
        console.log(`Enabling bridge ${this.bridgeName}`);
        this.bridgeUp();
      }
    } catch (e) {
      console.error('Error enabling bridge: ', e);
    } finally {
      process.exit(0);
    }
  }
}
