const commandExists = require('command-exists-promise');
const { snapshot } = require('process-list');
import { exec } from 'child_process';
import { promises as fs, constants as fsConstants } from 'fs';
import * as path from 'path';

export class SystemInteropUtils {
  public static async getRunningProcesses(): Promise<Array<TaskInfo>> {
    const tasks = await snapshot('pid', 'name');
    return tasks;
  }

  public static async throwIfFileCantBeCreated(...files: string[]) {
    for (const file of files) {
      if (!(await SystemInteropUtils.canCreateFile(file))) {
        throw new Error(`The log file at ${path.resolve(file)} cannot be created.`);
      }
    }
  }

  public static async canCreateFile(filepath: string): Promise<boolean> {
    const dirFromFile = path.resolve(path.dirname(filepath));
    try {
      await fs.access(dirFromFile, fsConstants.W_OK);
      return true;
    } catch (e) {
      return false;
    }
  }

  public static async canReadFile(filepath: string): Promise<boolean> {
    try {
      await fs.access(filepath, fsConstants.R_OK);
      return true;
    } catch (e) {
      return false;
    }
  }

  public static async commandOutputMatchesRegex(command: string, regex: RegExp) {
    const output = await SystemInteropUtils.execPromise(command);
    if (output.stdout.match(regex)) {
      return true;
    }
    return false;
  }

  public static execPromise(command: string) {
    return new Promise<{ stdout: string; stderr: string }>((resolve, reject) => {
      exec(command, (error, stdout, stderr) => {
        if (error) {
          reject(error);
        } else {
          resolve({ stdout, stderr });
        }
      });
    });
  }

  public static commandExists(command: string): Promise<boolean> {
    return commandExists(command);
  }

  public static async readFilesInDirectory(directoryPath: string, extensionsToInclude: string[] | undefined = undefined) {
    const files = await fs.readdir(directoryPath);

    if (extensionsToInclude) {
      return files.filter((file) =>
        extensionsToInclude.some((extension) => extension.toLowerCase() === path.extname(file).toLowerCase().replace('.', ''))
      );
    }

    return files;
  }
}

export interface TaskInfo {
  name: string;
  pid: number;
}
