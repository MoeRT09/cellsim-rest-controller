# Cellsim REST-Controller

## Table of Contents

[[_TOC_]]

## Introduction

With this tool it is possible to control the trace-based network emulator [Cellsim](https://github.com/keithw/multisend) via an REST-interface.

## Prerequisites

For network emulation with Cellsim, you need to have at least two network interfaces on your system. Cellsim will act as an Ethernet bridge and will delay packets going through them according to a trace file (traces can be found [here](https://github.com/ravinet/mahimahi/tree/master/traces)).
Those two network interfaces should not be configured with any IP-address! If Cellsim is not running, no traffic will be forwarded between the interfaces, leaving devices connected to them without network connectivity. You can manually create a network bridge between the interfaces. The Cellsim REST-Controller can automatically stop the bridge, if an emulation is running (Cellsim will bridge them) and start it again if Cellsim is stopped (the system will bridge the interfaces). For Ubuntu, an Ethernet bridge between two network interfaces can be created with the following commands:

```bash
sudo apt install bridge-utils       # Install user space parts for setting up bridges
sudo brctl addbr br0                # Add a bridge called "br0"
sudo brctl addif br0 <INTERFACE_1>  # Add the NIC with the name "INTERFACE_1" to the bridge
sudo brctl addif br0 <INTERFACE_2>  # Add the NIC with the name "INTERFACE_2" to the bridge
ifconfig br0 up                     # Start bridging the NICs
```

For the Cellsim REST-Controller, you need to have Node.js and it's packet manager npm installed.

## How to run

1. You need the Cellsim binary. It can be compiled by using the code from the [original repository](https://github.com/keithw/multisend):
   1. Clone the repository: `git clone https://github.com/keithw/multisend.git`
   2. Switch your working directory to the `sender` subdirectory of the repository: `cd multisend sender`.
   3. Compile the binary: `make cellsim`. The executable will be available in the same directory.  
      Note: You need to have a build environment present on your system for the compilation to work. For Ubuntu or Debian you can install the required tools with the command `sudo apt install build-essential`.
2. Clone this repositroy: `git clone https://gitlab.com/MoeRT09/cellsim-rest-controller.git`
3. Change working directory: `cd cellsim-rest-controller`
4. Install required dependencies: `npm install`
5. Adjust the config in `app/config.jsonc` file according to your environment.
6. Run the application (needs root privileges, as cellsim won't work otherwise): `sudo npm run start`. You can specify an alternative config file with the parameter `--config`. Example: `sudo npm run start -- --config my-config.jsonc`.

## API-Documentation

### **Start emulation**

Start network emulation with Cellsim. If the controller is configured accordingly, it will automatically disable an Ethernet bridge.

- **URL**

  /start?uplinkTrace=:uplinkTrace&downlinkTrace=:downlinkTrace&lossrate=:lossrate&logTag=:logTag

- **Method:**

  `POST`

- **URL Params**

  **Required:**

  - `uplinkTrace=[string]`
  - `downlinkTrace=[string]`

  **Optional:**

  - `lossrate=[number]`
  - `logTag=[string]`

- **Success Response:**

  - **Code:** 200 <br />
    **Content:** `{ "running": true, "uplinkTrace": "/home/administrator/saturator/traces/Verizon-LTE-driving.up", "downlinkTrace": "/home/administrator/saturator/traces/Verizon-LTE-driving.down", "lossrate": 0 }`
  - **Code:** 200 <br />
    **Content:** `{ "running": true, "uplinkTrace": "/home/administrator/saturator/traces/Verizon-LTE-driving.up", "downlinkTrace": "/home/administrator/saturator/traces/Verizon-LTE-driving.down", "lossrate": 0.2, "logTag": "packet loss test" }`

- **Error Response:**

  - **Code:** 400 BAD_REQUEST <br />
    **Content:** `{ error: "Can't open the downlink trace file." }`

  OR

  - **Code:** 400 BAD_REQUEST <br />
    **Content:** `{error: 'The lossrate must be a value between 0 and 1.'}`

  OR

  - **Code:** 400 BAD_REQUEST <br />
    **Content:** `{ error: 'Specify an up- and downlink trace as follows: "/start?uplinkTrace=trace.up&downlinkTrace=trace.down". Issue "/traces" to get a list of available choices.' }`
  
  OR

  - **Code:** 405 METHOD_NOT_ALLOWED <br />
    **Content:** `empty`

  OR

  - **Code:** 500 INTERNAL_SERVER_ERROR <br />
    **Content:** `{"error":"Cellsim is already running (PID: 9864). Only one instance can run at the time."}`

- **Sample Call:**

  `curl -X POST http://<host>/start\?uplinkTrace\=Verizon-LTE-driving.up\&downlinkTrace\=Verizon-LTE-driving.down`

### **Stop emulation**

Stop network emulation with Cellsim. If the controller is configured accordingly, it will automatically start an Ethernet bridge.

- **URL**

  /stop

- **Method:**

  `PUT`

- **URL Params**

  None

- **Success Response:**

  - **Code:** 200 <br />
    **Content:** `{"running":false,"exitCode":0}`

  - **Code:** 200 <br />
    **Content:** `{"running":false}`

- **Error Response:**

  - **Code:** 405 METHOD_NOT_ALLOWED <br />
    **Content:** `empty`

  OR

  - **Code:** 500 INTERNAL_SERVER_ERROR <br />
    **Content:** `{"error": <some arbitrary error>}`

- **Sample Call:**

  `curl -x PUT http://<host>/stop`

### **Get the running status of Cellsim**

Get information whether Cellsim is currently running.

- **URL**

  /status

- **Method:**

  `GET`

- **URL Params**

  None

- **Success Response:**

  - **Code:** 200 <br />
    **Content:** `{"running":false}`

  - **Code:** 200 <br />
    **Content:** `{"running":true}`

- **Error Response:**

  - **Code:** 405 METHOD_NOT_ALLOWED <br />
    **Content:** `empty`

  OR

  - **Code:** 500 INTERNAL_SERVER_ERROR <br />
    **Content:** `{"error": <some arbitrary error>}`

- **Sample Call:**

  `curl http://<host>/status`

### **Get available traces**

Get a list of available trace files that Cellsim can use

- **URL**

  /traces

- **Method:**

  `GET`

- **URL Params**

  None

- **Success Response:**

  - **Code:** 200 <br />
    **Content:** `[ "ATT-LTE-driving-2016.down", "ATT-LTE-driving-2016.up", "ATT-LTE-driving.down", "ATT-LTE-driving.up" ]`

- **Error Response:**

  - **Code:** 405 METHOD_NOT_ALLOWED <br />
    **Content:** `empty`

  OR

  - **Code:** 500 INTERNAL_SERVER_ERROR <br />
    **Content:** `{"error": <some arbitrary error>}`

- **Sample Call:**

  `curl http://<host>/traces`
